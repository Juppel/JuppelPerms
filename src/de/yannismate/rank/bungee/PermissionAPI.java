package de.yannismate.rank.bungee;

import de.yannismate.rank.api.MySQL;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Yannis on 31.10.2016.
 */
public class PermissionAPI {

    static Plugin plugin;
    static MySQL mysql = new MySQL("78.94.12.214:9999");

    static Set<String> adminperms = new HashSet<>();
    static Set<String> developerperms = new HashSet<>();
    static Set<String> srmodperms = new HashSet<>();
    static Set<String> modperms = new HashSet<>();
    static Set<String> supporterperms = new HashSet<>();
    static Set<String> headbuilderperms = new HashSet<>();
    static Set<String> builderperms = new HashSet<>();
    static Set<String> youtuberperms = new HashSet<>();
    static Set<String> freundperms = new HashSet<>();
    static Set<String> crafterperms = new HashSet<>();
    static Set<String> spielerperms = new HashSet<>();

    static void loadPermissions(){
        ProxyServer.getInstance().getScheduler().runAsync(plugin, new Runnable() {
            @Override
            public void run() {
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    adminperms.clear();
                    developerperms.clear();
                    srmodperms.clear();
                    modperms.clear();
                    supporterperms.clear();
                    headbuilderperms.clear();
                    builderperms.clear();
                    youtuberperms.clear();
                    freundperms.clear();
                    crafterperms.clear();
                    srmodperms.clear();
                    spielerperms.clear();
                    ResultSet rs = mysql.getQuery("SELECT * FROM perms");
                    while(rs.next()){
                        String permission = rs.getString(2);
                        String rank = rs.getString(1);
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Admin.toString())){
                            adminperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Developer.toString())){
                            developerperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.SrMod.toString())){
                            srmodperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Mod.toString())){
                            modperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.HeadBuilder.toString())){
                            headbuilderperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Builder.toString())){
                            builderperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Supporter.toString())){
                            supporterperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Crafter.toString())){
                            crafterperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.Freund.toString())){
                            freundperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(de.yannismate.rank.bukkit.Rank.YouTuber.toString())){
                            youtuberperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase("Spieler")){
                            spielerperms.add(permission);
                            adminperms.add(permission);
                            srmodperms.add(permission);
                            developerperms.add(permission);
                            modperms.add(permission);
                            headbuilderperms.add(permission);
                            builderperms.add(permission);
                            supporterperms.add(permission);
                            crafterperms.add(permission);
                            freundperms.add(permission);
                            youtuberperms.add(permission);
                        }
                    }
                    rs.close();
                    System.out.println("[JuppelPerms] Permissions aktualisiert!");
                }catch(Exception e){
                    System.out.println("[JuppelPerms] Fehler beim Laden der Permissions -> " + e.getMessage());
                }
            }
        });
    }


}
