package de.yannismate.rank.bungee;

import de.yannismate.rank.api.MySQL;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Yannis on 31.10.2016.
 */
public class COMMAND_setrank extends Command {

    public COMMAND_setrank(String name, Plugin plugin) {
        super(name, "permissions.setrank");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 2){
            String rank = args[1];
            try{
                Rank setrank = Rank.getByString(rank);
                if(ProxyServer.getInstance().getPlayer(args[0]) != null){
                    String uuid = ProxyServer.getInstance().getPlayer(args[0]).getUniqueId().toString();
                    setRank(uuid, setrank);
                    sender.sendMessage(new TextComponent("§7[§cSystem§7] §aDu §ahast §aden §aRank §e" + rank.toString() + " §avergeben!"));
                    ProxyServer.getInstance().getPlayer(args[0]).disconnect(new TextComponent("§cDu §chast §cden §cRang §e" + rank.toString() + " §cerhalten!"));
                    Rank.updateRankBackground(uuid);
                    return;
                }else{
                    try{
                        String uuid = UUIDFetcher.getUUID(args[0]);
                        setRank(uuid, setrank);
                        Rank.updateRankBackground(uuid);
                        sender.sendMessage(new TextComponent("§7[§cSystem§7] §aDu §ahast §aden §aRank §e" + rank.toString() + " §avergeben!"));
                        return;
                    }catch(Exception e){
                        sender.sendMessage(new TextComponent("§7[§cSystem§7] §cUUID §ckonnte §cnicht §cabgerufen §cwerden!"));
                        return;
                    }
                }
            }catch(Exception e){
                BaseComponent bs = new TextComponent("§7[§cSystem§7] §cFalscher §cRang.\n§7[§cSystem§7]");
                TextComponent ranks = new TextComponent(" Mögliche Ränge: Admin, Developer, SrMod, Mod. Supporter, HeadBuilder, Builder, YouTuber, Freund, Crafter, Spieler.");
                ranks.setColor(ChatColor.RED);
                bs.addExtra(ranks);
                sender.sendMessage(bs);
                return;
            }
        }else{
            sender.sendMessage(new TextComponent("§7[§cSystem§7] §cNutze §e/setrank §e[User] §e[Rank]§c!"));
            return;
        }

    }


    static void setRank(String uuid, Rank rank){
        ProxyServer.getInstance().getScheduler().runAsync(PermissionAPI.plugin, new Runnable() {
            @Override
            public void run() {
                MySQL mysql = PermissionAPI.mysql;
                if(mysql.con == null){
                    mysql.login();
                }
                if(rank == Rank.Spieler){
                    mysql.executeUpdate("DELETE FROM ranks WHERE uuid='" + uuid + "'");
                    return;
                }
                mysql.executeUpdate("INSERT INTO ranks (rank, uuid) VALUES ('" + rank.toString() + "','" + uuid + "') ON DUPLICATE KEY UPDATE rank = '" + rank.toString() + "';");
            }
        });
    }
}
