package de.yannismate.rank.bungee;

import de.yannismate.rank.api.MySQL;
import de.yannismate.rank.bukkit.*;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Yannis on 31.10.2016.
 */
public class BungeeMain extends Plugin{


    static MySQL mysql = new MySQL("78.94.12.214:9999");

    @Override
    public void onEnable(){
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new COMMAND_addperm("addperm"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this,new COMMAND_removeperm("removeperm"));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new COMMAND_setrank("setrank", this));
                ProxyServer.getInstance().getPluginManager().registerListener(this, new PermissionEvent(this));
        System.out.println("[JuppelPermissions] Aktiviert!");
        ProxyServer.getInstance().getScheduler().runAsync(this, new Runnable() {
            @Override
            public void run() {
                if(mysql.con == null){
                    mysql.login();
                }
                mysql.executeUpdate("CREATE TABLE IF NOT EXISTS ranks (`uuid` VARCHAR(50), `rank` VARCHAR(50));");
                mysql.executeUpdate("CREATE TABLE IF NOT EXISTS perms (`rank` VARCHAR(50), `permission` VARCHAR(50));");
            }
        });
        Rank.loadRanks();
        PermissionAPI.loadPermissions();
    }


}
