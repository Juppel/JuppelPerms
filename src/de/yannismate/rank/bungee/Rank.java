package de.yannismate.rank.bungee;

import de.yannismate.rank.api.MySQL;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Yannis on 31.10.2016.
 */
public enum Rank {

    Admin,
    Developer,
    SrMod,
    Mod,
    Supporter,
    HeadBuilder,
    Builder,
    YouTuber,
    Freund,
    Crafter,
    Spieler;


    @Override
    public String toString() {
        switch(this) {
            case Admin:
                return "Admin";
            case Developer:
                return "Developer";
            case SrMod:
                return "SrMod";
            case Mod:
                return "Mod";
            case Supporter:
                return "Supporter";
            case HeadBuilder:
                return "HeadBuilder";
            case Builder:
                return "Builder";
            case YouTuber:
                return "YouTuber";
            case Freund:
                return "Freund";
            case Crafter:
                return "Crafter";
            case Spieler:
                return "Spieler";
            default: throw new IllegalArgumentException();
        }
    }




    static Plugin plugin;

    static Set<String> admin = new HashSet<>();
    static Set<String> developer = new HashSet<>();
    static Set<String> srmod = new HashSet<>();
    static Set<String> mod = new HashSet<>();
    static Set<String> supporter = new HashSet<>();
    static Set<String> headbuilder = new HashSet<>();
    static Set<String> builder = new HashSet<>();
    static Set<String> youtuber = new HashSet<>();
    static Set<String> freund = new HashSet<>();
    static Set<String> crafter = new HashSet<>();

    static Rank getRank(String uuid){
        if(admin.contains(uuid)){
            return Rank.Admin;
        }
        if(developer.contains(uuid)){
            return Rank.Developer;
        }
        if(srmod.contains(uuid)){
            return Rank.SrMod;
        }
        if(mod.contains(uuid)){
            return Rank.Mod;
        }
        if(supporter.contains(uuid)){
            return Rank.Supporter;
        }
        if(headbuilder.contains(uuid)){
            return Rank.HeadBuilder;
        }
        if(builder.contains(uuid)){
            return Rank.Builder;
        }
        if(youtuber.contains(uuid)){
            return Rank.YouTuber;
        }
        if(freund.contains(uuid)){
            return Rank.Freund;
        }
        if(crafter.contains(uuid)){
            return Rank.Crafter;
        }
        else{
            return Rank.Spieler;
        }
    }

    static MySQL mysql = new MySQL("78.94.12.214:9999");

    static void loadRanks(){
        ProxyServer.getInstance().getScheduler().runAsync(plugin, new Runnable() {
            @Override
            public void run() {
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    ResultSet rs = mysql.getQuery("SELECT * FROM ranks");
                    admin.clear();
                    developer.clear();
                    srmod.clear();
                    mod.clear();
                    headbuilder.clear();
                    builder.clear();
                    supporter.clear();
                    crafter.clear();
                    freund.clear();
                    youtuber.clear();

                    while(rs.next()){
                        String uuid = rs.getString(1);
                        String rank = rs.getString(2);
                        if(rank.equalsIgnoreCase(Admin.toString())){
                            admin.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Developer.toString())){
                            developer.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(SrMod.toString())){
                            srmod.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Mod.toString())){
                            mod.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(HeadBuilder.toString())){
                            headbuilder.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Builder.toString())){
                            builder.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Supporter.toString())){
                            supporter.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Crafter.toString())){
                            crafter.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Freund.toString())){
                            freund.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(YouTuber.toString())){
                            youtuber.add(uuid);
                        }
                    }
                    rs.close();
                    System.out.println("[JuppelPermissions] Ränge neu geladen!");
                }catch(Exception e){
                    System.out.println("ERROR! [Perms] --> " + e.getMessage());
                }

            }
        });
    }

    static void updateRank(String uuid, Rank rank2){
        String rank = rank2.toString();
        admin.remove(uuid);
        developer.remove(uuid);
        srmod.remove(uuid);
        mod.remove(uuid);
        headbuilder.remove(uuid);
        builder.remove(uuid);
        supporter.remove(uuid);
        crafter.remove(uuid);
        freund.remove(uuid);
        youtuber.remove(uuid);
        if(rank.equalsIgnoreCase(Admin.toString())){
            admin.add(uuid);
        }
        if(rank.equalsIgnoreCase(Developer.toString())){
            developer.add(uuid);
        }
        if(rank.equalsIgnoreCase(SrMod.toString())){
            srmod.add(uuid);
        }
        if(rank.equalsIgnoreCase(Mod.toString())){
            mod.add(uuid);
        }
        if(rank.equalsIgnoreCase(HeadBuilder.toString())){
            headbuilder.add(uuid);
        }
        if(rank.equalsIgnoreCase(Builder.toString())){
            builder.add(uuid);
        }
        if(rank.equalsIgnoreCase(Supporter.toString())){
            supporter.add(uuid);
        }
        if(rank.equalsIgnoreCase(Crafter.toString())){
            crafter.add(uuid);
        }
        if(rank.equalsIgnoreCase(Freund.toString())){
            freund.add(uuid);
        }
        if(rank.equalsIgnoreCase(YouTuber.toString())){
            youtuber.add(uuid);
        }
    }

    static void updateRankBackground(String uuid){
       ProxyServer.getInstance().getScheduler().runAsync(plugin, new Runnable() {
            @Override
            public void run() {
                Rank oldrank = getRank(uuid);
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    ResultSet rs = mysql.getQuery("SELECT rank FROM ranks WHERE uuid='" + uuid + "'");
                    Rank newrank = Rank.Spieler;
                    if(rs.next()) {
                        newrank = Rank.getByString(rs.getString(1));
                    }
                    if(newrank != oldrank){
                        updateRank(uuid, newrank);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }



    static boolean hasPermission(String permission, Rank rank){
        if(rank == Rank.Admin){
            if(PermissionAPI.adminperms.contains(permission)){
                return true;
            }
            else{
                return false;
            }
        }
        if(rank == Rank.Developer){
            if(PermissionAPI.developerperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        if(rank == Rank.SrMod){
            if(PermissionAPI.srmodperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        if(rank == Rank.Mod){
            if(PermissionAPI.modperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        if(rank == Rank.HeadBuilder){
            if(PermissionAPI.headbuilderperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        if(rank == Rank.Builder){
            if(PermissionAPI.builderperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }

        if(rank == Rank.Supporter){
            if(PermissionAPI.supporterperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }

        if(rank == Rank.Crafter){
            if(PermissionAPI.crafterperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }

        if(rank == Rank.Freund){
            if(PermissionAPI.freundperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }

        if(rank == Rank.Spieler){
            if(PermissionAPI.spielerperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        if(rank == Rank.YouTuber){
            if(PermissionAPI.youtuberperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
        else{
            if(PermissionAPI.spielerperms.contains(permission)){
                return true;
            }else{
                return false;
            }
        }
    }




    static Rank getByString(String s) throws Exception{
        String rank = s;
        if(s.equalsIgnoreCase("Admin")){
            return Rank.Admin;
        }
        if(s.equalsIgnoreCase("Developer") || s.equalsIgnoreCase("dev")){
            return Rank.Developer;
        }
        if(s.equalsIgnoreCase("SrMod")){
            return Rank.SrMod;
        }
        if(s.equalsIgnoreCase("Mod")){
            return Rank.Mod;
        }
        if(s.equalsIgnoreCase("Supporter")){
            return Rank.Supporter;
        }
        if(s.equalsIgnoreCase("HeadBuilder")){
            return Rank.HeadBuilder;
        }
        if(s.equalsIgnoreCase("Builder")){
            return Rank.Builder;
        }
        if(s.equalsIgnoreCase("Crafter")){
            return Rank.Crafter;
        }
        if(s.equalsIgnoreCase("YouTuber")){
            return Rank.YouTuber;
        }
        if(s.equalsIgnoreCase("Freund")){
            return Rank.Freund;
        }
        if(s.equalsIgnoreCase("Spieler")){
            return Rank.Spieler;
        }else{
            throw new Exception("Wrong Rank type");
        }
    }

}
