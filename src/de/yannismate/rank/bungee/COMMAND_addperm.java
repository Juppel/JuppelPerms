package de.yannismate.rank.bungee;

import de.yannismate.rank.api.MySQL;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Yannis on 31.10.2016.
 */
public class COMMAND_addperm extends Command {

    public COMMAND_addperm(String name) {
        super(name, "permissions.juppel");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 2){
            String rank = args[0];
            try {
                Rank setrank = Rank.getByString(rank);
                if(Rank.hasPermission(args[1], setrank)){
                    sender.sendMessage(new TextComponent("§7[§cSystem§7] §cDieser §cRang §chat §cdie §cPermission §cbereits!"));
                    return;
                }else{
                    addPermission(setrank, args[1]);
                    sender.sendMessage(new TextComponent("§7[§cSystem§7] §aDer §aRang §e" + setrank.toString() + " §ahat §adie §aPermission §e" + args[1] + " §aerhalten!"));
                    return;
                }

            }catch(Exception e){
            BaseComponent bs = new TextComponent("§7[§cSystem§7] §cFalscher §cRang.\n§7[§cSystem§7]");
            TextComponent ranks = new TextComponent(" Mögliche Ränge: Admin, Developer, SrMod, Mod. Supporter, HeadBuilder, Builder, YouTuber, Freund, Crafter, Spieler.");
            ranks.setColor(ChatColor.RED);
            bs.addExtra(ranks);
            sender.sendMessage(bs);
            return;
        }
        }
        else{
            sender.sendMessage(new TextComponent("§7[§cSystem§7] §cNutze §e/addperm §e[Rank] §e[§7(-)§ePermission]§c!"));
            return;
        }
    }


    static void addPermission(Rank rank, String permission){
        ProxyServer.getInstance().getScheduler().runAsync(PermissionAPI.plugin, new Runnable() {
            @Override
            public void run() {
                MySQL mysql = PermissionAPI.mysql;
                if(mysql.con == null){
                    mysql.login();
                }
                mysql.executeUpdate("INSERT INTO perms (rank, permission) VALUES('" + rank.toString() + "','" + permission + "')");
                PermissionAPI.loadPermissions();
            }
        });
    }
}
