package de.yannismate.rank.api;

/**
 * Created by Yannis on 31.10.2016.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL


    //

{
    public Connection con;
    private String USER = "";
    private String PASSWORD = "";
    private String ipandport;

    public MySQL(String ipandport) {
        this.USER = "mc76";
        this.PASSWORD = "c0d0060177";
        this.ipandport = ipandport;
    }

    public void login() {
        try {
            this.con = DriverManager.getConnection("jdbc:mysql://" + ipandport + "/mc76?autoReconnect=true", this.USER, this.PASSWORD);
        } catch (SQLException e) {
            System.err.println("[Permissions] MySQL -> Fehler beim Verbinden!" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void logout() {
        try {
            if ((getConnection() == null) && (!getConnection().isClosed())) {
                getConnection().close();
            }
        } catch (SQLException e) {
            login();
            System.err.println("[Permissions] MySQL -> Fehler beim Trennen der Verbindung!");
            System.err.println("[Permissions] MySQL -> " + e.getMessage());
        }
    }

    public Connection getConnection() {
        return this.con;
    }

    public void executeUpdate(String query) {
        try {
            Statement stm = getConnection().createStatement();
            stm.executeUpdate(query);
            stm.close();
        } catch (SQLException e) {
            System.err.println("[Permissions] MySQL -> Fehler beim Update!");
            System.err.println("[Permissions] MySQL -> " + e.getMessage());
        }
    }

    public ResultSet getQuery(String query) {
        Statement statement = null;
        try {
            statement = getConnection().createStatement();
            return statement.executeQuery(query);
        } catch (SQLException e) {
            System.err.println("[Permissions] MySQL -> Fehler bei der Query!");
            System.err.println("[Permissions] MySQL -> " + e.getMessage());
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
}
