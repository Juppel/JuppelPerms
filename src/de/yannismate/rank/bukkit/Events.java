package de.yannismate.rank.bukkit;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

/**
 * Created by Yannis on 31.10.2016.
 */
public class Events implements Listener{

    static Plugin plugin;
    public Events(Main plugin){
        Events.plugin = plugin;
        Rank.plugin = plugin;
        PermissionAPI.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onJoin(PlayerJoinEvent event){
        Rank.updateRankBackground(event.getPlayer().getUniqueId().toString());
        PermissionAPI.setPermissions(event.getPlayer());
        if(Rank.getRank(event.getPlayer().getUniqueId().toString()) == Rank.Admin){
            event.getPlayer().setOp(true);
            event.getPlayer().sendMessage("§aDir wurde automatisch OP gegeben!");
        }
    }

}
