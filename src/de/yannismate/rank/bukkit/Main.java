package de.yannismate.rank.bukkit;

import de.yannismate.rank.api.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Yannis on 31.10.2016.
 */
public class Main extends JavaPlugin {

    static MySQL mysql = new MySQL("10.0.0.99:3306");

    @Override
    public void onEnable(){
        Bukkit.getPluginManager().registerEvents(new Events(this), this);
        Bukkit.getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                if(mysql.con == null){
                    mysql.login();
                }
                mysql.executeUpdate("CREATE TABLE IF NOT EXISTS ranks (`uuid` VARCHAR(50), `rank` VARCHAR(50));");
                mysql.executeUpdate("CREATE TABLE IF NOT EXISTS perms (`rank` VARCHAR(50), `permission` VARCHAR(50));");
            }
        });
        PermissionAPI.loadPermissions();
        Rank.loadRanks();
        startTimer();
    }


    public void startTimer(){
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                PermissionAPI.loadPermissions();
            }
        }, 20*120, 20*120);
    }

}
