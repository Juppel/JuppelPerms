package de.yannismate.rank.bukkit;

import de.yannismate.rank.api.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Yannis on 31.10.2016.
 */
public enum Rank {

    Admin,
    Developer,
    SrMod,
    Mod,
    Supporter,
    HeadBuilder,
    Builder,
    YouTuber,
    Freund,
    Crafter,
    Spieler;

    static Plugin plugin;

    static Set<String> admin = new HashSet<>();
    static Set<String> developer = new HashSet<>();
    static Set<String> srmod = new HashSet<>();
    static Set<String> mod = new HashSet<>();
    static Set<String> supporter = new HashSet<>();
    static Set<String> headbuilder = new HashSet<>();
    static Set<String> builder = new HashSet<>();
    static Set<String> youtuber = new HashSet<>();
    static Set<String> freund = new HashSet<>();
    static Set<String> crafter = new HashSet<>();

    @Override
    public String toString() {
        switch(this) {
            case Admin:
                return "Admin";
            case Developer:
                return "Developer";
            case SrMod:
                return "SrMod";
            case Mod:
                return "Mod";
            case Supporter:
                return "Supporter";
            case HeadBuilder:
                return "HeadBuilder";
            case Builder:
                return "Builder";
            case YouTuber:
                return "YouTuber";
            case Freund:
                return "Freund";
            case Crafter:
                return "Crafter";
            case Spieler:
                return "Spieler";
            default: throw new IllegalArgumentException();
        }
    }


    static Rank getRank(String uuid){
        if(admin.contains(uuid)){
            return Rank.Admin;
        }
        if(developer.contains(uuid)){
            return Rank.Developer;
        }
        if(srmod.contains(uuid)){
            return Rank.SrMod;
        }
        if(mod.contains(uuid)){
            return Rank.Mod;
        }
        if(supporter.contains(uuid)){
            return Rank.Supporter;
        }
        if(headbuilder.contains(uuid)){
            return Rank.HeadBuilder;
        }
        if(builder.contains(uuid)){
            return Rank.Builder;
        }
        if(youtuber.contains(uuid)){
            return Rank.YouTuber;
        }
        if(freund.contains(uuid)){
            return Rank.Freund;
        }
        if(crafter.contains(uuid)){
            return Rank.Crafter;
        }
        else{
            return Rank.Spieler;
        }
    }

    static MySQL mysql = new MySQL("10.0.0.99:3306");

    static void loadRanks(){
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    ResultSet rs = mysql.getQuery("SELECT * FROM ranks");
                    admin.clear();
                    developer.clear();
                    srmod.clear();
                    mod.clear();
                    headbuilder.clear();
                    builder.clear();
                    supporter.clear();
                    crafter.clear();
                    freund.clear();
                    youtuber.clear();

                    while(rs.next()){
                        String uuid = rs.getString(1);
                        String rank = rs.getString(2);
                        if(rank.equalsIgnoreCase(Admin.toString())){
                            admin.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Developer.toString())){
                            developer.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(SrMod.toString())){
                            srmod.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Mod.toString())){
                            mod.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(HeadBuilder.toString())){
                            headbuilder.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Builder.toString())){
                            builder.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Supporter.toString())){
                            supporter.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Crafter.toString())){
                            crafter.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(Freund.toString())){
                            freund.add(uuid);
                        }
                        if(rank.equalsIgnoreCase(YouTuber.toString())){
                            youtuber.add(uuid);
                        }
                    }
                    rs.close();
                    Bukkit.getConsoleSender().sendMessage("[JuppelPermissions] Ränge neu geladen!");
                }catch(Exception e){
                    Bukkit.getConsoleSender().sendMessage("ERROR! [Perms] --> " + e.getMessage());
                }

            }
        });
    }

    static void updateRank(String uuid, Rank rank2){
        String rank = rank2.toString();
        admin.remove(uuid);
        developer.remove(uuid);
        srmod.remove(uuid);
        mod.remove(uuid);
        headbuilder.remove(uuid);
        builder.remove(uuid);
        supporter.remove(uuid);
        crafter.remove(uuid);
        freund.remove(uuid);
        youtuber.remove(uuid);
        if(rank.equalsIgnoreCase(Admin.toString())){
            admin.add(uuid);
        }
        if(rank.equalsIgnoreCase(Developer.toString())){
            developer.add(uuid);
        }
        if(rank.equalsIgnoreCase(SrMod.toString())){
            srmod.add(uuid);
        }
        if(rank.equalsIgnoreCase(Mod.toString())){
            mod.add(uuid);
        }
        if(rank.equalsIgnoreCase(HeadBuilder.toString())){
            headbuilder.add(uuid);
        }
        if(rank.equalsIgnoreCase(Builder.toString())){
            builder.add(uuid);
        }
        if(rank.equalsIgnoreCase(Supporter.toString())){
            supporter.add(uuid);
        }
        if(rank.equalsIgnoreCase(Crafter.toString())){
            crafter.add(uuid);
        }
        if(rank.equalsIgnoreCase(Freund.toString())){
            freund.add(uuid);
        }
        if(rank.equalsIgnoreCase(YouTuber.toString())){
            youtuber.add(uuid);
        }
        if(Bukkit.getPlayer(UUID.fromString(uuid)) != null){
            PermissionAPI.setPermissions(Bukkit.getPlayer(UUID.fromString(uuid)));
            Bukkit.getPlayer(UUID.fromString(uuid)).sendMessage("§aDeine Permissions wurden aktualisiert!");
        }
    }

    static void updateRankBackground(String uuid){
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                Rank oldrank = getRank(uuid);
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    ResultSet rs = mysql.getQuery("SELECT rank FROM ranks WHERE uuid = '" + uuid + "'");
                    Rank newrank = Rank.Spieler;
                    if(rs.next()){
                        String rank = rs.getString(1);
                        rs.close();
                        newrank = Rank.getByString(rank);
                    }else{
                        newrank = Rank.Spieler;
                    }

                    if(newrank != oldrank){
                        updateRank(uuid, newrank);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    static Rank getByString(String s) throws Exception{
        String rank = s;
        if(s.equalsIgnoreCase("Admin")){
            return Admin;
        }
        if(s.equalsIgnoreCase("Developer") || s.equalsIgnoreCase("dev")){
            return Developer;
        }
        if(s.equalsIgnoreCase("SrMod")){
            return SrMod;
        }
        if(s.equalsIgnoreCase("Mod")){
            return Mod;
        }
        if(s.equalsIgnoreCase("Supporter")){
            return Supporter;
        }
        if(s.equalsIgnoreCase("HeadBuilder")){
            return HeadBuilder;
        }
        if(s.equalsIgnoreCase("Builder")){
            return Builder;
        }
        if(s.equalsIgnoreCase("Crafter")){
            return Crafter;
        }
        if(s.equalsIgnoreCase("YouTuber")){
            return YouTuber;
        }
        if(s.equalsIgnoreCase("Freund")){
            return Freund;
        }
        if(s.equalsIgnoreCase("Spieler")){
            return Spieler;
        }else{
            throw new Exception("Wrong Rank type");
        }
    }

}
