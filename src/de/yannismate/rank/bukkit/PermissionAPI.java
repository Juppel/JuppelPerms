package de.yannismate.rank.bukkit;

import de.yannismate.rank.api.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Yannis on 31.10.2016.
 */
public class PermissionAPI {

    static Plugin plugin;
    static MySQL mysql = new MySQL("10.0.0.99:3306");

    static Set<String> adminperms = new HashSet<>();
    static Set<String> developerperms = new HashSet<>();
    static Set<String> srmodperms = new HashSet<>();
    static Set<String> modperms = new HashSet<>();
    static Set<String> supporterperms = new HashSet<>();
    static Set<String> headbuilderperms = new HashSet<>();
    static Set<String> builderperms = new HashSet<>();
    static Set<String> youtuberperms = new HashSet<>();
    static Set<String> freundperms = new HashSet<>();
    static Set<String> crafterperms = new HashSet<>();
    static Set<String> spielerperms = new HashSet<>();

    static void loadPermissions(){
        Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                try{
                    if(mysql.con == null){
                        mysql.login();
                    }
                    ResultSet rs = mysql.getQuery("SELECT * FROM perms");
                    while(rs.next()){
                        String permission = rs.getString(2);
                        String rank = rs.getString(1);
                        if(rank.equalsIgnoreCase(Rank.Admin.toString())){
                            adminperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Developer.toString())){
                            developerperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.SrMod.toString())){
                            srmodperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Mod.toString())){
                            modperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.HeadBuilder.toString())){
                            headbuilderperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Builder.toString())){
                            builderperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Supporter.toString())){
                            supporterperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Crafter.toString())){
                            crafterperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.Freund.toString())){
                            freundperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase(Rank.YouTuber.toString())){
                            youtuberperms.add(permission);
                        }
                        if(rank.equalsIgnoreCase("Spieler")){
                            spielerperms.add(permission);
                            adminperms.add(permission);
                            developerperms.add(permission);
                            srmodperms.add(permission);
                            modperms.add(permission);
                            headbuilderperms.add(permission);
                            builderperms.add(permission);
                            supporterperms.add(permission);
                            crafterperms.add(permission);
                            freundperms.add(permission);
                            youtuberperms.add(permission);
                        }
                    }
                    rs.close();
                    System.out.println("[JuppelPerms] Permissions aktualisiert!");
                }catch(Exception e){
                    Bukkit.getConsoleSender().sendMessage("[JuppelPerms] Fehler beim Laden der Permissions -> " + e.getMessage());
                }
            }
        });
    }

    static void setPermissions(Player player){
        for(PermissionAttachmentInfo ps : player.getEffectivePermissions()){
            try{
                player.removeAttachment(ps.getAttachment());
            }catch(Exception e){}
        }
        Rank rank = Rank.getRank(player.getUniqueId().toString());
        if(rank == Rank.Admin){
            for(String perms : adminperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Developer){
            for(String perms : developerperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.SrMod){
            for(String perms : srmodperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Mod){
            for(String perms : modperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Supporter){
            for(String perms : supporterperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.HeadBuilder){
            for(String perms : headbuilderperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Builder){
            for(String perms : builderperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.YouTuber){
            for(String perms : youtuberperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Freund){
            for(String perms : freundperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Crafter){
            for(String perms : crafterperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
        if(rank == Rank.Spieler){
            for(String perms : spielerperms){
                PermissionAttachment attachment = player.addAttachment(plugin);
                if(perms.startsWith("-")){
                    attachment.setPermission(perms, false);
                }else{
                    attachment.setPermission(perms, true);
                }
            }
        }
    }

}
